﻿using Android.App;
using Android.Content;
using Android.Media;
using Android.OS;
using Android.Util;

namespace PruebaFirebase.Firebase
{
    [Service]
    public class GCMIntentService : IntentService
    {
        static PowerManager.WakeLock sWakeLock;
        static object LOCK = new object();

        public static void RunIntentInService(Context context, Intent intent)
        {
            lock (LOCK)
            {
                if (sWakeLock == null)
                {
                    // This is called from BroadcastReceiver, there is no init.
                    var pm = PowerManager.FromContext(context);
                    sWakeLock = pm.NewWakeLock(
                    WakeLockFlags.Partial, "My WakeLock Tag");
                }
            }

            sWakeLock.Acquire();
            intent.SetClass(context, typeof(GCMIntentService));
            context.StartService(intent);
        }

        protected override void OnHandleIntent(Intent intent)
        {
            try
            {
                Context context = this.ApplicationContext;
                string action = intent.Action;

                if (action.Equals("com.google.android.c2dm.intent.REGISTRATION"))
                {
                    HandleRegistrationAsync(intent);
                }
                else if (action.Equals("com.google.android.c2dm.intent.RECEIVE"))
                {
                    HandleMessage(intent);
                }
            }
            finally
            {
                lock (LOCK)
                {
                    //Sanity check for null as this is a public method
                    if (sWakeLock != null) sWakeLock.Release();
                }
            }
        }

        private async void HandleRegistrationAsync(Intent intent)
        {
            string registrationId = intent.GetStringExtra("registration_id");
            string error = intent.GetStringExtra("error");
            string unregistration = intent.GetStringExtra("unregistered");

            if (string.IsNullOrEmpty(error))
            {
                await SNSUtils.RegisterDevice(SNSUtils.Platform.Android, registrationId);
                //var response = await SnsClient.CreatePlatformEndpointAsync(new CreatePlatformEndpointRequest
                //{
                //    Token = registrationId,
                //    PlatformApplicationArn = "YourPlatformArn" /* insert your platform application ARN here */
                //});
            }
        }

        private void HandleMessage(Intent intent)
        {
            string message = string.Empty;
            Bundle extras = intent.Extras;
            if (!string.IsNullOrEmpty(extras.GetString("message")))
            {
                message = extras.GetString("message");
            }
            else
            {
                message = extras.GetString("default");
            }

            Log.Info("Messages", "message received = " + message);
            ShowNotification(this, "SNS Push", message);
            //show the message

        }

        public void ShowNotification(GCMIntentService gCMIntentService,
        string contentTitle,
        string contentText)
        {
            // Intent
            Notification.Builder builder = new Notification.Builder(this)
              .SetContentTitle(contentTitle)
              .SetContentText(contentText)
              .SetDefaults(NotificationDefaults.Sound | NotificationDefaults.Vibrate)
              .SetSmallIcon(Resource.Drawable.abc_ic_clear_material)
              .SetSound(RingtoneManager.GetDefaultUri(RingtoneType.Notification));

            // Get the notification manager:
            NotificationManager notificationManager = this.GetSystemService(Context.NotificationService) as NotificationManager;

            notificationManager.Notify(1001, builder.Build());
        }
    }
}
