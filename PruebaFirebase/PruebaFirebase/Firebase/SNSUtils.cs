﻿using Amazon.CognitoIdentity;
using Amazon.Runtime;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using PruebaFirebase.Constants;
using System.Threading.Tasks;

namespace PruebaFirebase.Firebase
{
    public class SNSUtils
    {

        public enum Platform
        {
            Android,
            IOS,
            WindowsPhone
        }

        private static AWSCredentials _credentials;

        private static AWSCredentials Credentials
        {
            get
            {
                if (_credentials == null)
                    _credentials = new CognitoAWSCredentials(GlobalConfig.IdentityPoolId, GlobalConfig.CognitoRegion);
                return _credentials;
            }
        }

        private static IAmazonSimpleNotificationService _snsClient;

        private static IAmazonSimpleNotificationService SnsClient
        {
            get
            {
                if (_snsClient == null)
                    _snsClient = new AmazonSimpleNotificationServiceClient(Credentials, GlobalConfig.SnsRegion);
                return _snsClient;
            }
        }

        public static async Task RegisterDevice(Platform platform, string registrationId)
        {
            var arn = string.Empty;
            string _endpointArn = string.Empty;
            switch (platform)
            {
                case Platform.Android:
                    arn = GlobalConfig.AndroidPlatformApplicationArn;
                    break;
                case Platform.IOS:
                    arn = GlobalConfig.iOSPlatformApplicationArn;
                    break;
            }

            var response = await SnsClient.CreatePlatformEndpointAsync(new CreatePlatformEndpointRequest
            {
                Token = registrationId,
                PlatformApplicationArn = arn
            }
            );

            _endpointArn = response.EndpointArn;

        }

    }
}

