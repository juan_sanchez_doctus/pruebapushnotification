﻿using Android.App;
using Android.Widget;
using Android.OS;

using Android.Gms.Common;
using Firebase.Iid;
using Android.Util;
using Android.Support.V7.App;
using PruebaFirebase.Constants;
using Firebase.Messaging;
using Amazon;
using Android.Content;

namespace PruebaFirebase
{
    [Activity(Label = "PruebaFirebase", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : AppCompatActivity
    {

        static readonly string TAG = "MainActivity";

        internal static readonly string CHANNEL_ID = "my_notification_channel";
        internal static readonly int NOTIFICATION_ID = 100;

        private TextView msgText;
        private Button logTokenButton, suscribeToNotifications, unsuscribeToNotifications;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetTheme(Resource.Style.Theme_AppCompat_NoActionBar);
            SetContentView(Resource.Layout.Main);

            msgText = FindViewById<TextView>(Resource.Id.msgText);
            logTokenButton = FindViewById<Button>(Resource.Id.logTokenButton);
            suscribeToNotifications = FindViewById<Button>(Resource.Id.suscribeToNotifications);
            unsuscribeToNotifications = FindViewById<Button>(Resource.Id.unsuscribeToNotifications);

            if (Intent.Extras != null)
            {
                foreach (var key in Intent.Extras.KeySet())
                {
                    var value = Intent.Extras.GetString(key);
                    Log.Debug(TAG, "Key: {0} Value: {1}", key, value);
                }
            }

            //CON LAS DOS SIGUIENTES LINEAS CREAMOS EL CANAL DE NOTIFICACIONES CON FIREBASE
            //IsPlayServicesAvailable();
            //CreateNotificationChannel();

            logTokenButton.Click += delegate
            {
                //ESTE ES EL TOKEN ÚNICO CON EL QUE SE ENVIAN NOTIFICACIONES DESDE FIREBASE
                Log.Debug(TAG, "InstanceID token: " + FirebaseInstanceId.Instance.Token);
            };

            suscribeToNotifications.Click += delegate
            {
                FirebaseMessaging.Instance.SubscribeToTopic("news");
                Log.Debug(TAG, "Subscribed to remote notifications");
            };

            unsuscribeToNotifications.Click += delegate
            {
                FirebaseMessaging.Instance.UnsubscribeFromTopic("news");
                Log.Debug(TAG, "Unsubscribed from remote notifications");
            };

            //PARA REGISTRARSE CON GOOGLE CLOUD MESSAGING CON AWS
            RegisterForGCM();
        }

        protected override void OnResume()
        {
            base.OnResume();

            GlobalConfig.ActualActivity = this;

            Log.Debug(TAG, "google app id: " + GetString(Resource.String.google_app_id));

            var loggingConfig = AWSConfigs.LoggingConfig;
            loggingConfig.LogMetrics = true;
            loggingConfig.LogResponses = ResponseLoggingOption.Always;
            loggingConfig.LogMetricsFormat = LogMetricsFormatOption.JSON;
            loggingConfig.LogTo = LoggingOptions.SystemDiagnostics;

            AWSConfigs.AWSRegion = "us-east-1";

            var proxyConfig = AWSConfigs.ProxyConfig;
            proxyConfig.Host = "localhost";
            proxyConfig.Port = 80;
            proxyConfig.Username = "<username>";
            proxyConfig.Password = "<password>";

        }

        public bool IsPlayServicesAvailable()
        {
            int resultCode = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
            if (resultCode != ConnectionResult.Success)
            {
                if (GoogleApiAvailability.Instance.IsUserResolvableError(resultCode))
                    msgText.Text = GoogleApiAvailability.Instance.GetErrorString(resultCode);
                else
                {
                    msgText.Text = "This device is not supported";
                    Finish();
                }
                return false;
            }
            else
            {
                msgText.Text = "Google Play Services is available.";
                return true;
            }
        }

        void CreateNotificationChannel()
        {
            if (Build.VERSION.SdkInt < BuildVersionCodes.O)
            {
                // Notification channels are new in API 26 (and not a part of the
                // support library). There is no need to create a notification
                // channel on older versions of Android.
                return;
            }

            var channel = new NotificationChannel(CHANNEL_ID,
                                                  "FCM Notifications",
                                                  NotificationImportance.Default)
            {

                Description = "Firebase Cloud Messages appear in this channel"
            };

            var notificationManager = (NotificationManager)GetSystemService(Android.Content.Context.NotificationService);
            notificationManager.CreateNotificationChannel(channel);
        }

        private void RegisterForGCM()
        {
            string senders = GlobalConfig.GoogleConsoleProjectId;
            Intent intent = new Intent("com.google.android.c2dm.intent.REGISTER");
            intent.SetPackage("com.google.android.gsf");
            intent.PutExtra("app", PendingIntent.GetBroadcast(this, 0, new Intent(), 0));
            intent.PutExtra("sender", senders);
            StartService(intent);
        }
    }
}

