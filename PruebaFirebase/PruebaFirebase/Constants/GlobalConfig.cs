﻿using System;
using Amazon;
using Android.App;

namespace PruebaFirebase.Constants
{
    public class GlobalConfig
    {

        public static Activity ActualActivity { get; set; }
        public static String API_KEY = "AIzaSyBy_ryv-xyLgsomHWEwOE9lcgceyyCcaVQ";

        //identity pool id for cognito credentials
        public const string IdentityPoolId = "";

        public static RegionEndpoint CognitoRegion = RegionEndpoint.USEast1;
        public static RegionEndpoint SnsRegion = RegionEndpoint.USEast1;

        //sns android platform arn
        public const string AndroidPlatformApplicationArn = "arn:aws:sns:us-east-1:808000547196:app/GCM/PruebaFirebaseAndroid";

        //sns ios platform arn
        public const string iOSPlatformApplicationArn = "";

        //project id for android gcm
        public const string GoogleConsoleProjectId = "21e959ffaac16c1d6d62d3c317c9f1b266fb3171";
    }
}
